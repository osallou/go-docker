
__USER_QUOTA_MAPPING = (
    ('user_quota_time', 'total_time'),
    ('user_quota_cpu', 'total_cpu'),
    ('user_quota_ram', 'total_ram'),
    ('user_quota_gpu', 'total_gpu')
)

__PROJECT_QUOTA_MAPPING = (
    ('project_quota_time', 'total_time'),
    ('project_quota_cpu', 'total_cpu'),
    ('project_quota_ram', 'total_ram'),
    ('project_quota_gpu', 'total_gpu')
)


def __inc_usage(usage, requirements, duration):
    usage['total_time'] += duration
    usage['total_cpu'] += requirements['cpu'] * duration
    usage['total_ram'] += requirements['ram'] * duration
    usage['total_gpu'] += requirements.get('gpus', 0) * duration


def __is_quota_present(requirements, key_mapping):
    for key_r, key_u in key_mapping:
        if 0 < requirements.get(key_r, 0):
            return True
    return False


def __is_quota_exceeded(requirements, usage, key_mapping):
    for key_r, key_u in key_mapping:
        if 0 < requirements.get(key_r, 0) < usage[key_u]:
            return True
    return False


def is_quota_exceeded(task, get_usage, duration=0):
    '''
    Checks if quota exceeded

    :param task:
    :type task: dict
    :param get_usage: returns usage
    :type get_usage: callable
    :param duration: >0 if task was started
    :type duration: int
    :return:
    :rtype: boolean
    '''
    requirements = task['requirements']
    user = task['user']
    for key, kind, mapping in (
            ('id', 'user', __USER_QUOTA_MAPPING),
            ('project', 'group', __PROJECT_QUOTA_MAPPING)
    ):

        # if kind == 'group' and key == 'default':
        #     continue

        if __is_quota_present(requirements, mapping):
            usage = get_usage(user[key], kind)
            if duration:
                usage = dict(usage)  # make copy because 'get_usage' could return same dict again
                __inc_usage(usage, requirements, duration)
            if __is_quota_exceeded(requirements, usage, mapping):
                return True
    return False
