from godocker.IGoDockerPlugin import IGoDockerPlugin

import datetime
import time
import pymongo


class ISchedulerPlugin(IGoDockerPlugin):
    '''
    Scheduler plugins interface
    '''

    def features(self):
        '''
        Get supported features

        :return: list of features within ['docker-plugin-zfs']
        '''
        return []

    def reset(self):
        '''
        Reset scheduler plugin (connection, etc...)
        '''
        return

    def get_user_prio(self, user_id):
        '''
        Get user priority (between 0 and 1)
        '''
        prio = self.redis_handler.get(self.cfg['redis_prefix'] + ':user:' + user_id + ':prio')
        if prio is None:
            user = None
            for i in range(5):
                try:
                    user = self.users_handler.find_one({'id': user_id})
                    break
                except pymongo.errors.AutoReconnect:
                    self.logger.warn('Mongo:AutoReconnect')
                    time.sleep(pow(2, i))

            if not user or 'prio' not in user['usage']:
                prio = 0.5
            else:
                prio = float(user['usage']['prio'] / float(100))
                self.redis_handler.set(self.cfg['redis_prefix'] + ':user:' + user_id + ':prio', str(prio))
        else:
            prio = float(prio)
        return prio

    def get_project_prio(self, project_id):
        '''
        Get project priority (between 0 and 1)
        '''
        if project_id == 'default':
            return 0.5
        prio = self.redis_handler.get(self.cfg['redis_prefix'] + ':project:' + project_id + ':prio')
        if prio is None:
            project = None
            for i in range(5):
                try:
                    project = self.projects_handler.find_one({'id': project_id})
                    break
                except pymongo.errors.AutoReconnect:
                    self.logger.warn('Mongo:AutoReconnect')
                    time.sleep(pow(2, i))

            if not project or 'prio' not in project:
                prio = 0.5
            else:
                prio = float(project['prio'] / float(100))
                self.redis_handler.set(self.cfg['redis_prefix'] + ':project:' + project_id + ':prio', str(prio))
        else:
            prio = float(prio)
        return prio

    def get_bounds_waiting_time(self, tasks):
        '''
        Gets min and max waiting time for tasks

        :param tasks: list of pending tasks
        :type tasks: list
        :return: list (max_time, min_time)
        '''
        max_time = 0
        min_time = -1
        dt = datetime.datetime.now()
        timestamp = time.mktime(dt.timetuple())

        for task in tasks:
            delta = timestamp - task['date']
            if delta > max_time:
                max_time = delta
            if delta < min_time or min_time == -1:
                min_time = delta
        return (max_time, min_time)

    def get_bounds_usage(self, usages):
        '''
        Gets min and max usage

        :param usages: input usages list of dict{ total_time, total__cpu, total_ram }
        :type usages: list
        :return: dict {
                        max_time, min_time,
                        max_cpu, min_cpu,
                        max_ram, min_ram
                    }
        '''
        max_time = 0.0
        min_time = -1
        max_cpu = 0
        min_cpu = -1
        max_ram = 0
        min_ram = -1
        for usage in usages:
            if usage['total_time'] > max_time:
                max_time = usage['total_time']
            if usage['total_time'] < min_time or min_time == -1:
                min_time = usage['total_time']
            if usage['total_cpu'] > max_cpu:
                max_cpu = usage['total_cpu']
            if usage['total_cpu'] < min_cpu or min_cpu == -1:
                min_cpu = usage['total_cpu']
            if usage['total_ram'] > max_ram:
                max_ram = usage['total_ram']
            if usage['total_ram'] < min_ram or min_ram == -1:
                min_ram = usage['total_ram']

        return {
                'max_time': max_time,
                'min_time': min_time,
                'max_cpu': max_cpu,
                'min_cpu': min_cpu,
                'max_ram': max_ram,
                'min_ram': min_ram
        }

    def schedule(self, tasks):
        '''
        Schedule list of tasks to be ran according to user list

        :return: list of sorted tasks
        '''
        pass
