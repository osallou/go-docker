from yapsy.IPlugin import IPlugin
from bson.json_util import dumps
import json
import time
import pymongo
import redis
import datetime
from datetime import timedelta


class IGoDockerPlugin(IPlugin):
    '''
    Base plugin reference
    '''

    def redis_get_conn(self):
        '''
        Get a redis connection handler based on configuration

        Config parameters:
          * redis_socket_timeout [optional, default: 1 second]
          * redis_host [optinal if using sentinel]
          * redis_port [optinal if using sentinel]
          * redis_password [optional]

        Support Redis Sentinel to get master ip/port via redis_sentinel section

            redis_sentinel:
                hosts: 'sent1:26380,sent2:26380'
                service: 'godocker'

        :return: StrictRedis connection
        '''
        r_pwd = None
        if 'redis_password' in self.cfg:
            r_pwd = self.cfg['redis_password']
        sock_timeout = 1
        if 'redis_socket_timeout' in self.cfg:
            sock_timeout = self.cfg['redis_socket_timeout']
        master_ip = self.cfg['redis_host']
        master_port = self.cfg['redis_port']
        if 'redis_sentinel' in self.cfg:
            if 'hosts' in self.cfg['redis_sentinel'] and self.cfg['redis_sentinel']['hosts']:
                from redis.sentinel import Sentinel
                sentinel = Sentinel([tuple(address.split(':')) for address in self.cfg['redis_sentinel']['hosts'].split(',')])
                sentinel_master = 'godocker'
                if 'service' in self.cfg['redis_sentinel']:
                    sentinel_master = self.cfg['redis_sentinel']['service']
                (master_ip, master_port) = sentinel.discover_master(sentinel_master)
        r_con = redis.StrictRedis(host=master_ip,
            port=master_port,
            db=self.cfg['redis_db'],
            password=r_pwd,
            decode_responses=True,
            socket_timeout=sock_timeout)
        return r_con

    def redis_call(self, f, *args, **kwargs):
        '''
        Executes a redis call, and retries connection and command in case of failure.

        Example: redis_call(self.r.hset, field, keys, 1)
        '''
        count = 0
        max_retries = 5
        while True:
            try:
                return f(*args, **kwargs)
            except Exception as e:
                self.logger.exception(str(e))
                count += 1
                # re-raise the ConnectionError if we've exceeded max_retries
                if count > max_retries:
                    raise
                self.logger.warn('Redis:Retrying in {} seconds'.format(count))
                time.sleep(count)
                self.redis_handler = self.redis_get_conn()

    def set_redis_handler(self, redis_handler):
        self.redis_handler = redis_handler

    def set_jobs_handler(self, jobs_handler):
        self.jobs_handler = jobs_handler

    def set_users_handler(self, users_handler):
        self.users_handler = users_handler

    def set_projects_handler(self, projects_handler):
        self.projects_handler = projects_handler

    def get_name(self):
        '''
        Get name of plugin
        '''
        pass

    def set_config(self, cfg):
        '''
        Set configuration
        '''
        self.cfg = cfg

    def set_logger(self, logger):
        '''
        Set logger for logging
        '''
        self.logger = logger

    def get_users(self, user_id_list):
        '''
        Get users matching ids in user_id_list

        :param user_id_list: list containing the id of users
        :type user_id_list: list
        :return: list of users
        '''
        users = None
        for i in range(5):
            try:
                users = self.users_handler.find({'id': {'$in': user_id_list}})
                break
            except pymongo.errors.AutoReconnect:
                self.logger.warn('Mongo:AutoReconnect')
                time.sleep(pow(2, i))
        return users

    def get_group_usage(self, group_id):
        '''
        Get cpu/ram/time usage for last period for a group

        :param group_id: group identifier
        :type group_id: int
        :return: list (cpu,ram,duration)
        '''
        cpu = self.redis_handler.get(self.cfg['redis_prefix'] + ':group:' + str(group_id) + ':cpu')
        ram = self.redis_handler.get(self.cfg['redis_prefix'] + ':group:' + str(group_id) + ':ram')
        duration = self.redis_handler.get(self.cfg['redis_prefix'] + ':group:' + str(group_id) + ':time')
        return (cpu, ram, duration)

    def get_running_tasks(self, start=0, stop=-1):
        '''
        Get all tasks running

        :param start: first task index
        :type start: int
        :param stop: last task index (-1 = all)
        :type stop: int
        '''
        running_tasks = []
        tasks = self.redis_handler.lrange(self.cfg['redis_prefix'] + ':jobs:running', start, stop)
        for task in tasks:
            running_tasks.append(json.loads(task))
        return running_tasks

    def is_task_running(self, task_id):
        '''
        Checks if task is running

        :param task_id: task identifier
        :type task_id: int
        :return: bool
        '''
        task = self.redis_handler.get(self.cfg['redis_prefix'] + ':job:' + str(task_id) + ':task')
        if task is not None:
            return True
        return False

    def is_task_over(self, task_id):
        '''
        Checks if task is over

        :param task_id: task identifier
        :type task_id: int
        :return: bool
        '''
        task = self.db_jobsover.find_one({'id': task_id})
        if task is not None:
            return True
        return False

    def is_task_running_or_over(self, task_id):
        '''
        Checks if task is running or over

        :param task_id: task identifier
        :type task_id: int
        :return: bool
        '''
        if self.is_task_running(task_id):
            return True
        if self.is_task_over(task_id):
            return True
        return False

    def kill_tasks(self, task_list):
        '''
        Set input tasks in kill queue

        :param task_list: list of tasks
        :type task_list: list
        '''
        for task in task_list:
            for i in range(5):
                try:
                    self.jobs_handler.update({'id': task['id']},
                        {'$set': {
                                'status.secondary': 'kill requested'
                                }
                    })
                    break
                except pymongo.errors.AutoReconnect:
                    self.logger.warn('Mongo:AutoReconnect')
                    time.sleep(pow(2, i))

            self.redis_handler.rpush(self.cfg['redis_prefix'] + ':jobs:kill', dumps(task))

    def get_user_usage(self, identifier, key='user'):
        '''
        :param identifier: user/group identifier
        :type identifier: str
        :param key: user or group
        :type key: str
        :return: dict {
                total_time,
                total__cpu,
                total_ram,
                total_gpu
                }
        '''

        dt = datetime.datetime.now()

        total_time = 0.0
        total_cpu = 0.0
        total_ram = 0.0
        total_gpu = 0.0

        for i in range(0, self.cfg['user_reset_usage_duration']):
            previous = dt - timedelta(days=i)
            date_key = str(previous.year) + '_' + str(previous.month) + '_' + str(previous.day)
            if self.redis_handler.exists(self.cfg['redis_prefix'] + ':' + key + ':' + identifier + ':cpu:' + date_key):
                cpu = self.redis_handler.get(self.cfg['redis_prefix'] + ':' + key + ':' + identifier + ':cpu:' + date_key)
                gpu = self.redis_handler.get(self.cfg['redis_prefix'] + ':' + key + ':' + identifier + ':gpu:' + date_key)
                ram = self.redis_handler.get(self.cfg['redis_prefix'] + ':' + key + ':' + identifier + ':ram:' + date_key)
                duration = self.redis_handler.get(self.cfg['redis_prefix'] + ':' + key + ':' + identifier + ':time:' + date_key)
                if cpu:
                    total_cpu += float(cpu)
                if gpu:
                    total_gpu += float(gpu)
                if ram:
                    total_ram += float(ram)
                if duration:
                    total_time += float(duration)

        usage = {
                'total_time': total_time,
                'total_cpu': total_cpu,
                'total_ram': total_ram,
                'total_gpu': total_gpu
                }
        return usage
