from godocker.iWatcherPlugin import IWatcherPlugin
import time
import consul
import pymongo
import os


class TraefikWatcher(IWatcherPlugin):
    def get_name(self):
        return "traefikwatcher"

    def get_type(self):
        return "Watcher"

    def can_run(self, task):
        # Check if ip address available if not, but ok now, register
        # logging.error(json.dumps(task))
        # query redis, if ok now get task from self.jobs_handler to get ip address
        if 'ports' not in task['requirements']:
            return task
        ip_set = self.redis_call(self.redis_handler.get, self.cfg['traefik']['redis']['prefix'] + ':ip')
        if ip_set:
            return task
        extra_port = False
        for port in task['requirements']['ports']:
            if port == 22:
                continue
            extra_port = True
            break
        if not extra_port:
            return task

        job = None
        for i in range(5):
            try:
                job = self.jobs_handler.find_one({'id': task['id']})
                break
            except pymongo.errors.AutoReconnect:
                self.logger.warn('Mongo:AutoReconnect')
                time.sleep(pow(2, i))
        address = job['container'].get('ip_address', None)
        if not address:
            # ip address not yet available
            return task

        # Workaround for development where ip_address is *localhost*
        if os.environ.get('GOD_DEV_LOCALHOST_IP', None):
            address = os.environ['GOD_DEV_LOCALHOST_IP']

        consul_agent = consul.Consul(host=self.cfg['traefik']['consul']['host'])

        for port in task['container']['port_mapping']:
            if port['container'] == 22:
                continue
            uri = 'god-' + str(task['id']) + '-' + str(port['container']) + '.' + self.cfg['traefik']['domain']
            consul_agent.agent.service.register(
                'god-' + str(task['id']) + '-' + str(port['container']),
                service_id='god-' + str(task['id']) + '-' + str(port['container']),
                address=address,
                port=port['host'],
                tags=[
                    'godocker',
                    'traefik.backend=' + 'god-' + str(task['id']) + '-' + str(port['container']),
                    'traefik.frontend.rule=Host:' + uri,
                    'traefik.frontend.passHostHeader=true',
                    'traefik.enable=true'
                ]
            )
        self.redis_call(self.redis_handler.set, self.cfg['traefik']['redis']['prefix'] + ':ip', address)
        return task

    def done(self, task):
        '''
        Inform watcher of task termination

        :param task: current task
        :type task: Task
        '''
        # logging.error(json.dumps(task))
        ip_set = self.redis_call(self.redis_handler.get, self.cfg['traefik']['redis']['prefix'] + ':ip')
        if not ip_set:
            return task
        if 'ports' not in task['requirements']:
            return task
        consul_agent = consul.Consul(host=self.cfg['traefik']['consul']['host'])
        for port in task['requirements']['ports']:
            if port == 22:
                continue
            consul_agent.agent.service.deregister(
                'god-' + str(task['id']) + '-' + str(port)
            )
        self.redis_call(self.redis_handler.delete, self.cfg['traefik']['redis']['prefix'] + ':ip')
        return task
